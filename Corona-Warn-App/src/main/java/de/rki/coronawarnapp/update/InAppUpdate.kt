package de.rki.coronawarnapp.update

import android.app.Activity
import android.content.Context
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.suspendCancellableCoroutine
import timber.log.Timber
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

const val DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS = 3
const val UPDATE_AVAILABLE = 2
const val UPDATE_NOT_AVAILABLE = 1
const val UNKNOWN = 0

const val IMMEDIATE = 1
const val FLEXIBLE = 0

suspend fun <T> Task<T>.await(): T {
    return suspendCancellableCoroutine { cont ->
        addOnSuccessListener { info ->
            Timber.tag(TAG).d("OnSuccess info=$info")
            cont.resume(info as T)
        }
        addOnFailureListener {
            Timber.tag(TAG).e("OnFailure error=$it")
            cont.resumeWithException(it)
        }
    }
}

class AppUpdateManager{
    fun startUpdateFlowForResult(appUpdateInfo: AppUpdateInfo, immediate: Int, activity: Activity, updateCode: Int) {
        TODO("Not yet implemented")
    }
}
class AppUpdateInfo {
    fun updateAvailability(): Any {
        TODO("Not yet implemented")
    }
}

class AppUpdateManagerFactory {
    companion object {
        fun create(context: Context): AppUpdateManager {
            return AppUpdateManager()
        }
    }
}

suspend fun AppUpdateManager.getUpdateInfo(): AppUpdateInfo? {
    Timber.tag(TAG).d("getUpdateInfo()")
    return try {
        AppUpdateInfo()
    } catch (e: Exception) {
        Timber.tag(TAG).w(e, "getUpdateInfo() failed")
        null
    }
}

private const val TAG = "InAppUpdate"
