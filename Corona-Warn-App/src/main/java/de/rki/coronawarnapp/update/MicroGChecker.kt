package de.rki.coronawarnapp.update

import android.content.Context
import android.content.Intent
import android.net.Uri
import dagger.Reusable
import de.rki.coronawarnapp.appconfig.internal.ApplicationConfigurationCorruptException
import de.rki.coronawarnapp.util.ExposureNotificationProvider
import timber.log.Timber

@Reusable
class MicroGChecker(val context: Context) {
    fun checkForUpdate(): Result = try {
        if (isUpdateNeeded()) {
            Result(isUpdateNeeded = true, updateIntent = createUpdateAction())
        } else {
            Result(isUpdateNeeded = false)
        }
    } catch (exception: ApplicationConfigurationCorruptException) {
        Timber.e(
            "ApplicationConfigurationCorruptException caught:%s",
            exception.localizedMessage
        )

        Result(isUpdateNeeded = true, updateIntent = createUpdateAction())
    } catch (exception: Exception) {
        Timber.e("Exception caught:%s", exception.localizedMessage)
        Result(isUpdateNeeded = false)
    }

    private fun isUpdateNeeded() =
        ExposureNotificationProvider(context).getExposureNotificationProviderStatus() ==
            ExposureNotificationProvider.Status.EXTERNAL_UPDATE_REQUIRED

    private fun createUpdateAction(): () -> Intent = {
        Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(MICROG_DOWNLOAD_URL)
        }
    }

    data class Result(
        val isUpdateNeeded: Boolean,
        val updateIntent: (() -> Intent)? = null
    )

    companion object {
        private const val MICROG_DOWNLOAD_URL = "https://microg.org/download.html"
    }
}

